<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 3.12.2016
	 * Time: 22:50
	 */

	namespace Gali\DB;

	use Kdyby\Doctrine\Entities\BaseEntity;
	use Doctrine\ORM\Mapping as ORM;

	abstract class ConfigEntity extends BaseEntity implements IConfigEntity
	{

		/**
		 * @return mixed
		 */
		public abstract function getPath();
	}
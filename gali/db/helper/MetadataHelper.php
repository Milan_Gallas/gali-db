<?php

	namespace Gali\DB\Helper;

	use Gali\DB\IConfigEntity;

	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 3.12.2016
	 * Time: 23:25
	 */
	class MetadataHelper
	{
		/** @var \Kdyby\Doctrine\EntityManager */
		protected $entityManager;

		/** @var IConfigEntity */
		protected $entity;

		/**
		 * MetadataHelper constructor.
		 *
		 * @param \Kdyby\Doctrine\EntityManager $entityManager
		 */
		public function __construct(\Kdyby\Doctrine\EntityManager $entityManager)
		{
			$this->entityManager = $entityManager;
		}

		/**
		 * @param IConfigEntity $entity
		 */
		public function setEntity($entity)
		{
			$this->entity = $entity;
		}

		/**
		 * @return string
		 * @throws \Doctrine\ORM\Mapping\MappingException
		 */
		public function getPrimaryKey(){
			return $this->getEntityStructure()->getSingleIdentifierFieldName();
		}

		/**
		 * Vrátí strukturu vybrané entity
		 * @return \Doctrine\ORM\Mapping\ClassMetadata
		 */
		public function getEntityStructure()
		{
			return $this->entityManager->getClassMetadata($this->entity->getPath());
		}
	}
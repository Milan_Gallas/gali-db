<?php
	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 3.12.2016
	 * Time: 23:57
	 */

	namespace Gali\DB\Helper;

	use Doctrine\Common\Collections\Criteria;

	/**
	 * Pomocný třída pro filtrování nad jevyšším api
	 * Class CriteriaHelper
	 *
	 * @package Gali\DB\Helper
	 */
	class CriteriaHelper
	{
		CONST EQ = 'eq';
		CONST GT = 'gt';
		CONST LT = 'lt';
		CONST GTE = 'gte';
		CONST LTE = 'lte';
		CONST NEQ = 'neq';
		CONST IS_NULL = 'isNull';
		CONST IN = 'in';
		CONST NOT_IN = 'notIn';
		CONST CONTAINS = 'contains';

		/** @var Criteria */
		protected $criteria;

		/**
		 * sem se přidávájí setřídění
		 * @var array
		 */
		protected $orderings = array();

		/**
		 * Vytvoří prázdný object Criteria
		 */
		public function createCriteria()
		{
			$this->criteria = Criteria::create();
			return $this;
		}

		/**
		 * Vrátí výsledný dotaz pro entity manager
		 * @return Criteria
		 */
		public function getCriteria()
		{
			if ($this->orderings) {
				$this->criteria->orderBy($this->orderings);
			}
			return $this->criteria;
		}

		/**
		 * Nastvívím paginaci
		 * @param $limit - kolik jich chci vypsat
		 * @param $offset - kolik jich mám přeskočit
		 * @return $this
		 */
		public function entityLimit($limit, $offset, $presenterMaxItems = null)
		{
			if ($limit !== null && (is_null($presenterMaxItems) || $limit <= $presenterMaxItems)) {
				$this->criteria->setFirstResult($offset)->setMaxResults($limit);
			}
			return $this;
		}

		/**
		 * @param string $orderBy nazov sloupce
		 * @param bool $desc TRUE pokum má řadit jako DESC
		 * @return $this
		 */
		public function entityOrderby($orderBy, $desc = false)
		{
			if ($orderBy !== null) {
				$this->orderings[$orderBy] = $this->getOrderType($desc);
			}
			return $this;
		}

		/**
		 * Podle této metody zjistím, zda mám řadit sestupně, nebo vzestupně
		 * @param bool $desc
		 * @return string
		 */
		protected function getOrderType($desc)
		{
			if ($desc === false) {
				return Criteria::ASC;
			}
			return Criteria::DESC;
		}

		/**
		 * @param $columnName
		 * @param $value
		 * @param $comparisonType - vyber jednu z konstant
		 * @return $this
		 */
		public function entityAndWhere($columnName, $value, $comparisonType)
		{
			$this->criteria->andWhere(call_user_func(array(Criteria::expr(), $comparisonType), $columnName, $value));
			return $this;
		}

		/**
		 * @param $columnName
		 * @param $value
		 * @param $comparisonType
		 * @return $this
		 */
		public function entityOrWhere($columnName, $value, $comparisonType)
		{
			$this->criteria->orWhere(call_user_func(array(Criteria::expr(), $comparisonType), $columnName, $value));
			return $this;
		}
	}
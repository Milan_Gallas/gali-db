<?php

	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 3.12.2016
	 * Time: 22:02
	 */
	namespace Gali\DB;

	use Gali\DB\Helper\CriteriaHelper;
	use Gali\DB\Helper\MetadataHelper;
	use Kdyby\Doctrine\EntityManager;
	use Doctrine\Common\Collections\Criteria;

	class BaseFacade
	{
		/** @var EntityManager */
		protected $entityManager;

		/** @var  IConfigEntity rozhraní pro entity, které se budou zpracovávat touto fasádou */
		protected $entity;

		/** @var MetadataHelper */
		protected $metadataHelper;

		/** @var CriteriaHelper */
		protected $criteriaHelper;

		/**
		 * BaseFacade constructor.
		 *
		 * @param EntityManager $entityManager
		 */
		public function __construct(EntityManager $entityManager, MetadataHelper $metadataHelper)
		{
			$this->entityManager = $entityManager;
			$this->metadataHelper = $metadataHelper;
		}

		/**
		 * Pomocná třída
		 * @return MetadataHelper
		 */
		public function getMetadataHelper()
		{
			return $this->metadataHelper;
		}

		/**
		 * Vrátí všechny řádky z databáze
		 * @return array
		 */
		public function getAll()
		{
			return $this->entityManager->getRepository($this->entity->getPath())->findAll();
		}

		/**
		 * Vrátí konkrétní záznam z databáze
		 * @param integer $id
		 * @return null|object
		 */
		public function getEntity($id)
		{
			return $this->entityManager->getRepository($this->entity->getPath())->find($id);
		}

		/**
		 * Upraví danou entitu
		 * @throws \Exception
		 * @return $this
		 */
		public function updateEntity()
		{
			$this->entityManager->flush();
			return $this;
		}

		/**
		 * Vloží novou entitu
		 * @param IConfigEntity $entity
		 * @return $this
		 * @throws \Exception
		 */
		public function insertEntity(IConfigEntity $entity)
		{
			$this->entityManager->persist($entity);
			$this->entityManager->flush();
			return $this;
		}

		/**
		 * Smaže danou entitu
		 * @param IConfigEntity $entity
		 * @return $this
		 * @throws \Exception
		 */
		public function deleteEntity(IConfigEntity $entity)
		{
			$this->entityManager->remove($entity);
			$this->entityManager->flush();
			return $this;
		}

		/**
		 * Řekne fasádě se kterou entitou má pracovat
		 * @param IConfigEntity $entity
		 * @return $this
		 */
		public function setEntity(IConfigEntity $entity)
		{
			$this->entity = $entity;
			$this->metadataHelper->setEntity($entity);
			return $this;
		}

		/**
		 * @return integer Celkový počet řádků
		 */
		public function getTotalCount()
		{
			$query = $this->entityManager->createQuery("SELECT COUNT(e.{$this->metadataHelper->getPrimaryKey()}) FROM {$this->entity->getPath()} e");
			return $query->getSingleScalarResult();
		}

		/**
		 * Vrátí všechny vysledky z databáze, omezené paginací
		 * @param  $offset
		 * @param  $limit
		 * @return array
		 */
		public function getAllWithPagination($offset, $limit)
		{
			$criteria = Criteria::create()->setFirstResult($offset)->setMaxResults($limit);
			return $this->getByCriteria($criteria);
		}

		/**
		 * Metoda vrací data, které jsou omezeny za pomocí objectu Criteria
		 *
		 * @param \Doctrine\Common\Collections\Criteria $criteria
		 * @param boolean $toArray - pokud je uvedeno true, tak se výsledek vrátí jako pole hodnot
		 * @return array
		 * @throws \Exception
		 *
		 * Examples:
		 *      $criteria = Criteria::create()
		 *           ->where(Criteria::expr()->eq("birthday", "1982-02-17"))
		 *           ->orderBy(array("username" => Criteria::ASC))
		 *           ->setFirstResult(0)
		 *           ->setMaxResults(20)
		 *      ;
		 * $this->xxxManager->getByCriteria($criteria);
		 *
		 * pokud není zadán druhý parametr platí tento return
		 * @return \Doctrine\Common\Collections\Collection
		 */
		public function getByCriteria(Criteria $criteria, $toArray = false)
		{
			$result = $this->entityManager->getRepository($this->entity->getPath())->matching($criteria);
			if($toArray){
				return $result->toArray();
			}
			return $result;
		}
	}
<?php
	namespace Gali\DB;

	/**
	 * Created by PhpStorm.
	 * User: Milan Gallas
	 * Date: 3.12.2016
	 * Time: 22:51
	 */
	interface IConfigEntity
	{
		/**
		 * Vrátí cestu k dané entitě
		 * @return mixed
		 */
		public function getPath();
	}